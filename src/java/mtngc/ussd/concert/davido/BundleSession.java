/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;
import java.io.Serializable;
/**
 *
 * @author EKhosa
 */
public class BundleSession  implements Serializable {
    private String ussdSessionId;
    private String msisdn;
    private UserOption selectedUserOption;
    private int step;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

     /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the selectedUserOption
     */
    public UserOption getSelectedUserOption() {
        return selectedUserOption;
    }

    /**
     * @param selectedUserOption the selectedUserOption to set
     */
    public void setSelectedUserOption(UserOption selectedUserOption) {
        this.selectedUserOption = selectedUserOption;
    }
    
}
