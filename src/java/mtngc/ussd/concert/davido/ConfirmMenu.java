/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;

import java.util.Enumeration;

/**
 *
 * @author EKhosa
 */
public class ConfirmMenu {
    private UserOption selectedUserOption;
    static String CONFIRM = "1";
    static String CANCEL = "2";
   
    
    public ConfirmMenu(UserOption selectedUserOption){
        this.selectedUserOption=selectedUserOption;
    }
    
    
    public String getConfirmText(){
        StringBuilder sb = new StringBuilder();

        sb.append(selectedUserOption.getConfirmText());        
        sb.append("\n");
        sb.append("1 Confirm\n");
        sb.append("2 Cancel\n");
        sb.append("Respond");
        
        String str = sb.toString();
                
        return str;
    }



    /**
     * @return the segment
     */
    public UserOption getSelectedUserOption() {
        return selectedUserOption;
    }

    /**
     * @param segment the segment to set
     */
//    public void setSelectedOption(UserOption selectedUserOption) {
//        this.selectedUserOption= selectedUserOption;
//    }
    
    

    
}
