/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;

/**
 *
 * @author EKhosa
 */
public class FeedbackMenu {
    private UserOption userOption;
 
   
    
    public FeedbackMenu(UserOption userOption){
        this.userOption=userOption;
        
    }
    
    
    public String getSuccessText(){
        StringBuilder sb = new StringBuilder();
        sb.append("Congratulations, your participation in the draw has been validated. ");
        sb.append("You have been charged "+userOption.getPrice()+" F. The more you play, the more you increase your chances of winning.");
        
        String str = sb.toString();  
        return str;      
    }

    public String getInsufficientBalanceText(){
        StringBuilder sb = new StringBuilder();
        sb.append("You do not have enough credit to participate in this game. Please recharge your account and try again");
        
        String str = sb.toString();  
        return str;      
    }
    
    public String getNotAllowedText(){
        StringBuilder sb = new StringBuilder();
        sb.append("You are not allowed to participate in this game. Please call 111 for more info");
        
        String str = sb.toString();  
        return str;      
    }    
}
