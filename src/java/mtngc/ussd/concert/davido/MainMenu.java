
package mtngc.ussd.concert.davido;
import mtngc.ussd.concert.davido.UserOption;
import java.io.Serializable;
import java.util.*;
/**
 *
 * @author EKhosa
 */
public class MainMenu implements Serializable  {
    private LinkedHashMap userOptions ;
    private String msg;
    
    public MainMenu(String msisdnStr)  {
        Init(msisdnStr);
    }
    
    private void Init(String msisdnStr) {        
          
                                               
        userOptions = new LinkedHashMap();
        

        UserOption userOption = new UserOption();
        userOption.setPrice(1000);
        userOption.setOptionEnum(OptionEnum.TICKET);
	userOption.setOptionText("Entrance ticket");
        userOption.setConfirmText("You will be charged "+userOption.getPrice()+" F to participate in the draw and try to win a ticket for Davido's concert");
        userOptions.put("1", userOption);
        
        userOption = new UserOption();
        userOption.setPrice(1000);
        userOption.setOptionEnum(OptionEnum.PHOTO);
	userOption.setOptionText("Photo with Davido");
        userOption.setConfirmText("You will be charged "+userOption.getPrice()+" F to participate in the draw and try to win a shooting photo with Davido");
        userOptions.put("2", userOption);

        userOption = new UserOption();
        userOption.setPrice(2000);
        userOption.setOptionEnum(OptionEnum.DINER);
	userOption.setOptionText("Dinner with Davido");
        userOption.setConfirmText("You will be charged "+userOption.getPrice()+" F to participate in the draw and try to win a Dinner with Davido");
        userOptions.put("3", userOption);
        
        userOption = new UserOption();
        userOption.setPrice(1000);
        userOption.setOptionEnum(OptionEnum.TSHIRT);
	userOption.setOptionText("Davido T-shirt");
        userOption.setConfirmText("You will be charged "+userOption.getPrice()+" F to participate in the draw and try to win a T-shirt signed Davido for Davido's concert");
        userOptions.put("4", userOption);       

        Process();              
               
    }
    

    
    public String getString(){
        return msg;
    }
        
  
    public void Process(){
        StringBuilder sb = new StringBuilder();
        sb.append("Davido Concert \n");
	sb.append("Try to win by draw\n");
        
        
               
        // Get a set of the entries
        Set set = userOptions.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            String key = (String) me.getKey();
            UserOption userOption = (UserOption) me.getValue();
            sb.append(key+" - "+userOption.getOptionText()+"\n");
        }
              
        sb.append("\n");
        sb.append("Respond");
        msg = sb.toString();
        
    }
    
    public UserOption getOption(String input){
        UserOption segment = null;
        Object obj = userOptions.get(input);
        if(obj != null){
            segment = (UserOption) obj;
        }
        
        return segment;       
    } 
    
    
    
}

