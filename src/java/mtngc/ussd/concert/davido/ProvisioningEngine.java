/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;

import ucipclient.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import dbaccess.dbsrver.*;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;


/**
 *
 * @author EKhosa
 */
public class ProvisioningEngine {
    
    
    
    private static int[] supportedServiceClasses = {20, 6,19,66,88, 5, 15, 30, 3, 2, 4, 16, 28,12,40};
    
    
    
    
    ResponseEnum Execute(String msisdn, String transactionId, UserOption option, String cellId,String region){
        ResponseEnum respEnum  = ResponseEnum.SUCCESS;      
        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"| Fetching Service Class and Balance details");
                           
        
        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        if(getAccountResponse != null){
            int sc = getAccountResponse.getServiceClass();
            getAccountResponse.getBalance();
            if(this.IsServiceClassAllowed(sc)){
                double balanceBefore = getAccountResponse.getBalance();
                int price = option.getPrice();
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price:"+price+"|Balance:"+balanceBefore+"|Serviced Class "+sc+" Ok. Performing Balance check");
                    
                if(price > balanceBefore){
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session "+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|Insufficient Funds");
                        
                    respEnum = ResponseEnum.INSUFFCIENT_BALANCE;
                }else{
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|Funds are sufficient");
                        
                    String externalData1 = "DAVIDO";
                    UCIPUpdateBalanceAndDateResponse deductMAResponse 
                            = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, -price,true, new Date(),externalData1);
                    int x = deductMAResponse.getResponseCode();
                    if((x == 0) && (!deductMAResponse.isError())){
                        double balanceAfter = deductMAResponse.getBalance();
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|New Balance: "+balanceAfter+"|Success");
                        
                        respEnum  = ResponseEnum.SUCCESS;
                        persist(msisdn, transactionId, option, balanceBefore, balanceAfter, cellId, region);
                        
                    }else {
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|Response Code: "+x+"| System failed to deduct funds");
                        respEnum  = ResponseEnum.ERROR; 
                    }
                    
                }
            }else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|Service Class "+sc+" is  not supported");
                    
                respEnum  = ResponseEnum.NOT_ALLOWED;
            }
        }else {
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+option.getOptionEnum().toString()+"|Price: "+option.getPrice()+"|Failed to get account details");
                
                
            respEnum  = ResponseEnum.ERROR; 
        }       
        
        return respEnum;    
    }
    
    private int persist(String msisdn, String transactionId, UserOption option,double balanceBefore,double balanceAfter,String cellId,String region){
        int msisdnNum = Integer.parseInt(msisdn);
        int price = option.getPrice();
        String entryType = option.getOptionEnum().toString();
        DavidoDBClient dbClient = new DavidoDBClient();
        int insertEntry = dbClient.insertEntry( msisdnNum,transactionId,entryType,price, balanceBefore,balanceAfter,cellId,region);
        
          
        return 0;
    
    }
   
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
     
    
}
