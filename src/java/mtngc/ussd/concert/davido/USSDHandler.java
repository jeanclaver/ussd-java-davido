/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
/**
 *
 * @author EKhosa
 */
public class USSDHandler {
    
    private String ContextKeyName = "Davido";
    
    public USSDHandler(){
    }
    
    static
    {  
        MLogger.setHomeDirectory("C:\\logs\\davido");
        MLogger.Log("USSDHandler", LogLevel.ALL, "Static Initialization");         
    }  
    
    public void HandleRequest(HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
        String FreeFlow = "FC";
        
        try {
            String msisdn = request.getParameter("MSISDN");
            //msisdn = "224660134178";
            try{msisdn = msisdn.substring(3);}catch(Exception n){}
            String ussdSessionid = request.getParameter("SESSIONID");
            String newrequest = request.getParameter("newrequest");
            String input = request.getParameter("INPUT");
            
            String cellId = request.getParameter("cellid");            
            String region = request.getParameter("region");
            
            ServletContext context = request.getServletContext();
                   
                       
            StringBuilder sb = new StringBuilder();
            

                
            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Processing request ");
            String contextKey = GetContextKey(ussdSessionid);
            Object obj = context.getAttribute(contextKey);
            if(obj == null){
                String str = DisplayMainMenu(context, ussdSessionid, msisdn);
                sb.append(str );
            }else {

                BundleSession bundleSession = (BundleSession) obj;
                UserOption seg = bundleSession.getSelectedUserOption();
                if (seg == null){// if segment was not previously selected, it means it is a main menu response
                    if((input != null) && ( !input.trim().equals(""))){
                        input = input.trim();
                        MainMenu mainMenu = new MainMenu(msisdn);
                        seg = mainMenu.getOption(input);
                        if (seg != null){//user entered a VALIED input
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+seg.getOptionEnum().toString()+" selected. User to confirm");
                            bundleSession.setSelectedUserOption(seg);
                            context.setAttribute(contextKey, bundleSession);
                            ConfirmMenu confirmMenu = new ConfirmMenu(seg);
                            String str = confirmMenu.getConfirmText();
                            sb.append(str);                        
                        }else{//user entered an invalid input
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Invalid input "+input);
                        
                            String str = DisplayMainMenu(context, ussdSessionid, msisdn);
                            sb.append(str);
                        }                        
                    }else{
                        //naughty user, he didnt enter anything
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Empty input");
                        
                        String str = DisplayMainMenu(context, ussdSessionid, msisdn);
                        sb.append(str);
                    }                    
                }else {// if segment was previously selected, it means it is a confirmation response
                    if((input != null) && ( !input.trim().equals(""))){
                        input = input.trim();

                        if (input.equals(ConfirmMenu.CONFIRM)){
                            //PROVISION!!!!!!!!!!!!
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+seg.getOptionEnum().toString()+" confirmed.");
            
                            ProvisioningEngine provEngine = new ProvisioningEngine();
                                
                            ResponseEnum respEnum = provEngine.Execute(msisdn, ussdSessionid, seg, cellId,region);

                            //ResponseEnum respEnum = ResponseEnum.SUCCESS;
                            FeedbackMenu fdMenu = new FeedbackMenu(seg);
                            String str = "";

                            if(respEnum == ResponseEnum.SUCCESS){
                                str = fdMenu.getSuccessText();
                            }else if(respEnum == ResponseEnum.INSUFFCIENT_BALANCE){
                                str = fdMenu.getInsufficientBalanceText();
                            }else if(respEnum == ResponseEnum.NOT_ALLOWED){
                                str = fdMenu.getNotAllowedText();
                            }else if(respEnum == ResponseEnum.ERROR){
                                str = "System error";
                            }else{
                                str = "Unspecified error";
                            }                           

                            sb.append(str);
                            Cleanup(context, ussdSessionid);
                            FreeFlow = "FB";                                
                        }else if (input.equals(ConfirmMenu.CANCEL)){// user wants to select again
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+seg.getOptionEnum().toString()+"|Cancelled.");
                            String str = DisplayMainMenu(context, ussdSessionid, msisdn);  
                            sb.append(str); 
                        }else{//user entered invalid confirmation input
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+seg.getOptionEnum().toString()+"|Invalid input "+input);
                            
                            ConfirmMenu confirmMenu = new ConfirmMenu(seg);
                            String str = confirmMenu.getConfirmText();
                            sb.append(str);                                
                        }                        
                    }else{
                        //naughty user, he didnt enter anything
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+seg.getOptionEnum().toString()+"|Empty input");
                            
                        String str = DisplayMainMenu(context, ussdSessionid, msisdn);
                        sb.append(str);
                    }
                }

            }
                
            
            
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", FreeFlow);
            response.setHeader("cpRefId", "emk2545");
            //response.setContentType(type);
            PrintWriter out = response.getWriter();
//            out.append("Your string goes here\n");
//            out.append("Your string goes here");
            
            out.append(sb.toString());
           
            out.close();
        
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
            
        
    }
    
    private String DisplayMainMenu(ServletContext context, String ussdSessionid, String msisdn){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Main menu");
        Cleanup( context, ussdSessionid);    
        
        BundleSession bundleSession = new BundleSession();     
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setMsisdn(msisdn);
        String contextKey = GetContextKey(ussdSessionid);
        context.setAttribute(contextKey, bundleSession);
        
        
        MainMenu mainMenu = new MainMenu(msisdn);
        //mainMenu.Init(msisdn);
        String str = mainMenu.getString();
        
        return str;
    }
    private void Cleanup(ServletContext context, String ussdSessionid){
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
    }

    public void TraceParametersAndHeaders(HttpServletRequest request){
        
        Enumeration<String> headerEnum = request.getHeaderNames();
        MLogger.Log(this, LogLevel.DEBUG, "Tracing HTTP Headers and Parameters ");
        
        while(headerEnum.hasMoreElements()) {
            String headerName = headerEnum.nextElement();
            String headerValue = request.getHeader(headerName);
            //System.out.println("Header:- "+headerName+": "+headerValue);
            MLogger.Log(this, LogLevel.DEBUG, "Header:- "+headerName+": "+headerValue);
        } 
        
        Map<String, String[]> map = request.getParameterMap();
        //Reading the Map
        //Works for GET && POST Method
        for(String paramName:map.keySet()) {
            String[] paramValues = map.get(paramName);

            //Get Values of Param Name
            for(String valueOfParam:paramValues) {
                //Output the Values
                //System.out.println("Value of Param with Name "+paramName+": "+valueOfParam);
                //ystem.out.println("Parameter:- "+paramName+": "+valueOfParam);
                MLogger.Log(this, LogLevel.DEBUG, "Parameter:- "+paramName+": "+valueOfParam);
                
            }
        }           
        
    }
    
    
    private String GetContextKey(String ussdSessionid){
        return ContextKeyName+"-"+ussdSessionid;
    } 
       
        
}