/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.concert.davido;
import java.io.Serializable;


/**
 *
 * @author EKhosa
 */
public class UserOption implements Serializable {
    private int price;
    private OptionEnum optionEnum;
    private String optionText;
    private String confirmText;

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the option
     */
    public OptionEnum getOptionEnum() {
        return optionEnum;
    }

    /**
     * @param option the option to set
     */
    public void setOptionEnum(OptionEnum optionEnum) {
        this.optionEnum = optionEnum;
    }

    /**
     * @return the optionText
     */
    public String getOptionText() {
        return optionText;
    }

    /**
     * @param optionText the optionText to set
     */
    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    /**
     * @return the confirmText
     */
    public String getConfirmText() {
        return confirmText;
    }

    /**
     * @param confirmText the confirmText to set
     */
    public void setConfirmText(String confirmText) {
        this.confirmText = confirmText;
    }
    
}
